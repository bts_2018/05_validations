export class Book {
  
    public id:number ;
    public title:string;
    public author:string;
    public url:string;
  
}